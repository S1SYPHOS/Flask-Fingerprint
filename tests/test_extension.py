import os
import json

from flask import Flask, url_for

from flask_fingerprint import Fingerprint


# Global setup
# (1) Base directory
base_dir = os.path.dirname(os.path.abspath(__file__))

# (2) Manifest
manifest_file = os.path.join(base_dir, 'fixtures/manifest.json')

# (3) Fixtures
with open(manifest_file, 'r') as file:
    fixtures = json.load(file)


def test_manifest():
    # Run function
    app = Flask(__name__)
    app.config['rev_manifest'] = manifest_file
    Fingerprint(app)

    # See https://code.luasoftware.com/tutorials/flask/things-you-should-know-about-flask-server-name/#url_for
    with app.app_context(), app.test_request_context():
        for key, value in fixtures.items():
            # Assert result
            assert url_for('static', filename=key)  # == '/static/' + value


def test_prefixed_manifest():
    # Setup
    # (1) Manifest
    manifest_file = os.path.join(base_dir, 'fixtures/prefixed.json')

    # (2) Fixtures
    with open(manifest_file, 'r') as file:
        prefixed = json.load(file)

    # Run function
    app = Flask(__name__)
    Fingerprint(app, {'manifest': manifest_file})

    with app.app_context(), app.test_request_context():
        for key, value in prefixed.items():
            # Assert result
            assert url_for('static', filename=os.path.relpath(key, 'static')) == '/' + value


def test_no_manifest():
    # Run function
    app = Flask(__name__)
    Fingerprint(app)

    with app.app_context(), app.test_request_context():
        for key, value in fixtures.items():
            # Assert result
            assert url_for('static', filename=key) == '/static/' + key


def test_invalid_manifest():
    # Run function
    app = Flask(__name__)
    app.config['rev_manifest'] = 'invalid/manifest'
    Fingerprint(app)

    with app.app_context(), app.test_request_context():
        for key, value in fixtures.items():
            # Assert result
            assert url_for('static', filename=key) == '/static/' + key


def test_fingerprinting():
    # Setup
    # (1) Tests
    tests = {
        'empty-image.jpg': 'empty-image.jpg',
        'scripts/main.js': 'scripts/main.6d09e670.js',
        'styles/main.css': 'styles/main.d41d8cd9.css',
    }

    # Run function
    app = Flask(__name__)
    Fingerprint(app)

    with app.app_context(), app.test_request_context():
        for key, value in tests.items():
            # Assert result
            assert url_for('static', filename=key) == '/static/' + value


def test_hash_size():
    # Setup
    # (1) Tests
    tests1 = {
        'empty-image.jpg': 'empty-image.jpg',
        'scripts/main.js': 'scripts/main.6d09.js',
        'styles/main.css': 'styles/main.d41d.css',
    }

    tests2 = {
        'empty-image.jpg': 'empty-image.jpg',
        'scripts/main.js': 'scripts/main.6d09e670b56ec10c.js',
        'styles/main.css': 'styles/main.d41d8cd98f00b204.css',
    }

    # (2) Configurations
    config1 = {'hash_size': 4}

    config2 = {'hash_size': 16}

    # Run function #1
    app1 = Flask(__name__)
    Fingerprint(app1, config1)

    with app1.app_context(), app1.test_request_context():
        for key, value in tests1.items():
            # Assert result #1
            assert url_for('static', filename=key) == '/static/' + value

    # Run function #2
    app2 = Flask(__name__)
    Fingerprint(app2, config2)

    with app2.app_context(), app2.test_request_context():
        for key, value in tests2.items():
            # Assert result #2
            assert url_for('static', filename=key) == '/static/' + value


def test_extensions():
    # Setup
    # (1) Tests
    tests1 = {
        'empty-image.jpg': 'empty-image.d41d8cd9.jpg',
        'scripts/main.js': 'scripts/main.6d09e670.js',
        'styles/main.css': 'styles/main.d41d8cd9.css',
    }

    tests2 = {
        'empty-image.jpg': 'empty-image.d41d8cd9.jpg',
        'scripts/main.js': 'scripts/main.js',
        'styles/main.css': 'styles/main.css',
    }

    # (2) Configurations
    config1 = {'extensions': []}

    config2 = {'extensions': ['jpg']}

    # Run function #1
    app1 = Flask(__name__)
    Fingerprint(app1, config1)

    with app1.app_context(), app1.test_request_context():
        for key, value in tests1.items():
            # Assert result #1
            assert url_for('static', filename=key) == '/static/' + value

    # Run function #2
    app2 = Flask(__name__)
    Fingerprint(app2, config2)

    with app2.app_context(), app2.test_request_context():
        for key, value in tests2.items():
            # Assert result #2
            assert url_for('static', filename=key) == '/static/' + value


def test_query():
    # Setup
    # (1) Tests
    tests = {
        'scripts/main.js': 'scripts/main.js?v=6d09e670',
        'styles/main.css': 'styles/main.css?v=d41d8cd9',
    }

    # Run function
    app = Flask(__name__)
    Fingerprint(app, {'query': True})

    with app.app_context(), app.test_request_context():
        for key, value in tests.items():
            # Assert result
            assert url_for('static', filename=key) == '/static/' + value


# For more information about common pitfalls when testing 'Flask' applications,
# see https://code.luasoftware.com/tutorials/flask/things-you-should-know-about-flask-server-name/#url_for
