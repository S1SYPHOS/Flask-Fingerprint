import pytest

from flask import Flask, render_template_string

from flask_fingerprint import Fingerprint


def create_app():
    # Create app
    app = Flask(__name__, static_folder='flask')

    # Add route
    @app.route('/')
    def index():
        return render_template_string("""
            <html>
                <head>
                    <title>Test</title>
                    <link href="{{ url_for('static', filename='css/main.css') }}">
                    <script src="{{ url_for('static', filename='js/main.js') }}">
                </head>
                <body>
                    <h1>Hello World!</h1>
                    <img src="{{ url_for('static', filename='empty-image.jpg') }}">
                </body>
            </html>
        """)

    # Activate extension
    Fingerprint(app)

    return app


@pytest.fixture()
def app():
    # Create application
    app = create_app()

    yield app


@pytest.fixture()
def client(app):
    return app.test_client()


def test_app(client):
    # Setup
    # (1) Tests
    tests = [
        'empty-image.jpg',
        'css/main.123.css',
        # 'js/main.123.js',
    ]

    # Run function
    response = client.get('/')

    # Assert result #1
    assert response.status_code == 200

    # Assert result #2
    for key in tests:
        assert bytes(key.encode()) in response.data


def test_assets(client):
    # Setup
    # (1) Tests
    tests = [
        'empty-image.jpg',
        'css/main.123.css',
        'js/main.123.js',
    ]

    for key in tests:
        # Run function
        response = client.get('/flask/' + key)

        # Assert result
        assert response.status_code == 200
