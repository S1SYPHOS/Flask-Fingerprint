from flask_fingerprint import get_hash, is_allowed


def test_get_hash():
    # Assert result #1
    assert get_hash('main.18jz2nd3m2n.jpeg') == '18jz2nd3m2n'

    # Assert result #2
    assert get_hash('styles/main.143dfg36.css') == '143dfg36'

    # Assert result #3
    assert get_hash('/path/to/scripts/main.h4h4.js') == 'h4h4'


def test_get_hash_empty():
    # Assert result #1
    assert get_hash('') == ''

    # Assert result #2
    assert get_hash('main.css') == ''

    # Assert result #3
    assert get_hash('/scripts/main.js') == ''


def test_is_allowed():
    # Assert result #1
    assert is_allowed('css', [])

    # Assert result #2
    assert is_allowed('css', ['css', 'js'])

    # Assert result #3
    assert not is_allowed('html', ['css', 'js'])

    # Assert result #4
    assert is_allowed('.Css', ['CSS', 'js'])

    # Assert result #5
    assert is_allowed('JPEG', 'jpeg')
